#ifndef LEDTIMER_H
#define LEDTIMER_H

#include <QObject>
#include <QTimerEvent>

class LedTimer : public QObject
{
private:
    int fd_seg;
    int m_nTimerId;
    bool light_dark;

protected:
    void timerEvent(QTimerEvent *event);

public:
    int led_n;
    LedTimer(QObject *parent=0);
    ~LedTimer();
    void show_led();
    void off_led();
    void show_seg(int dice1, int dice2, int total_dice);
};

#endif // LEDTIMER_H
