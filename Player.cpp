#include "Player.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

Player::Player()
{
    name = NULL;
    rounds = 1;
    balance = 30000;
    at_card_index = 0;
}

void Player::set_player_name(char *new_name)
{
    name = new_name;
}

char *Player::get_name() const
{
    return name;
}

void Player::add_round()
{
    rounds++;
}

int Player::get_round() const
{
    return rounds;
}

int Player::get_balance() const
{
    return balance;
}

int Player::get_owned_amount() const
{
    return cards_owned.size();
}

int Player::get_at_card_index() const
{
    return at_card_index;
}

bool Player::set_new_index(int new_index)
{
    int old_index = at_card_index;
    at_card_index = new_index;
    if (abs(new_index - old_index) > 19)
    {
        this->rounds++;
        this->balance += 1500;
        return true;
    }
    return false;
}

int Player::income(int amount)
{
    this->balance += amount;
    return balance;
}

int Player::expenses(int amount)
{
    this->balance -= amount;
    return balance;
}

void Player::buy_card(CityCard *card, int price_of_card)
{
    cards_owned.push_back(card);
    balance = balance - price_of_card;
}

void Player::sell_card(CityCard *card, int price_of_card)
{
    for (unsigned i=0; i<cards_owned.size(); ++i)
    {
        if(cards_owned[i] == card)
        {
            cards_owned.erase (cards_owned.begin()+i);
            balance = balance - price_of_card;
            return;
        }
    }
}

CityCard *Player::get_owned_cards(int *amount) const
{
    *amount = cards_owned.size();
    if(*amount!=0)
    {
        CityCard *ptr_cards = (CityCard *)&cards_owned[0];
        return ptr_cards;
    }
    return NULL;
}
