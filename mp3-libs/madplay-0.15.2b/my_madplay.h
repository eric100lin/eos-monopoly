#ifndef MYMADPLAY_H
#define MYMADPLAY_H

#ifdef  __cplusplus
extern  "C" {
#endif

//stopped==1 should stop playing immediately
int my_madplay_main(volatile int *stopped, int argc, char *argv[]);

#ifdef  __cplusplus
}
#endif

#endif // MYMADPLAY_H
