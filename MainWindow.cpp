#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QDesktopWidget>
#include <QTime>
#include <stdio.h>
#include <stdlib.h>

MainWindow::MainWindow(int n_player,
    char *player_names[4], char *player_chess[4],
    MusicThread *music_thread, QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow),
    bg_music_thread(music_thread), led_timer(parent)
{
    ui->setupUi(this);
    number_of_players = n_player;
    current_player_index = 0;

    enable_n_player_info();
    allocate_and_init_cards();
    create_players(player_names, player_chess);
    show_whos_turn();

#ifdef PLATFORM_PXA270
//    roll_dice_sound.set_music_name(ROLL_SOUND_NAME,true);
    set_board_square();
    showFullScreen();
#endif
}

MainWindow::~MainWindow()
{
    for(unsigned int i=0; i<N_CARDS; i++)
        delete cards[i];
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
#ifdef PLATFORM_PXA270
    bg_music_thread->stop();
    bg_music_thread->wait();
//    delete bg_music_thread;
#endif
    event->accept();
}

void MainWindow::create_players(char *player_names[4], char *player_chess[4])
{
    for(int i=0; i<number_of_players; i++)
        players[i].set_player_name(player_names[i]);
    QLabel *name_labels[] =
    {
        ui->player1_name, ui->player2_name,
        ui->player3_name, ui->player4_name
    };
    QLabel *chess_labels[] =
    {
        ui->lb_player1, ui->lb_player2,
        ui->lb_player3, ui->lb_player4
    };
    for(int i=0; i<number_of_players; i++)
    {
        chess_labels[i]->setPixmap(QPixmap(player_chess[i]));
        name_labels[i]->setText(players[i].get_name());
        pb_players[i] = new QPushButton();  //QString::number(i+1)
        pb_players[i]->setIcon(QPixmap(player_chess[i]));
        pb_players[i]->setIconSize(QSize(PLAYER_WIDTH,PLAYER_HEIGHT));
        show_pb_player(START_CARD_INDEX, i);
    }
}

void MainWindow::show_pb_player(int card_index, int player_index)
{
    QAbstractButton *pb_card = cards[card_index]->get_pb();
    pb_players[player_index]->setParent(pb_card);
    pb_players[player_index]->setGeometry(pb_card->x(), pb_card->y(),
                                          PLAYER_WIDTH, PLAYER_HEIGHT);
    if(player_index == 0)
        pb_players[0]->move(PLAYER_OFFSET,PLAYER_OFFSET);
    if(player_index == 1)
        pb_players[1]->move(PLAYER_OFFSET+PLAYER_WIDTH,PLAYER_OFFSET);
    else if(player_index == 2)
        pb_players[2]->move(PLAYER_WIDTH+PLAYER_OFFSET,PLAYER_HEIGHT+PLAYER_OFFSET);
    else if(player_index == 3)
        pb_players[3]->move(PLAYER_OFFSET,PLAYER_HEIGHT+PLAYER_OFFSET);

    pb_players[player_index]->show();
}

void MainWindow::enable_n_player_info()
{
    QString enable_style(PLAYER_INFO_ENABLE_STYLE);
    ui->player1_info->setStyleSheet(enable_style);
    if (number_of_players == 2)
    {
        ui->player2_info->setStyleSheet(enable_style);
        ui->player3_info->setEnabled(false);
        ui->player4_info->setEnabled(false);
    }
    else if (number_of_players == 3)
    {
        ui->player2_info->setStyleSheet(enable_style);
        ui->player3_info->setStyleSheet(enable_style);
        ui->player4_info->setEnabled(false);
    }
    else if (number_of_players == 4)
    {
        ui->player2_info->setStyleSheet(enable_style);
        ui->player3_info->setStyleSheet(enable_style);
        ui->player4_info->setStyleSheet(enable_style);
    }
}

void MainWindow::allocate_and_init_cards()
{
    Card **ptr_cards = &cards[0];
    *ptr_cards++ = new StartCard(ui->pb_left_down);         //START
    *ptr_cards++ = new CityCard(ui->pb_left_1, Europe, 8500, 1200, 4000, 5500, 7500, 9000, 7500, 7500, 4200);  //France
    *ptr_cards++ = new CityCard(ui->pb_left_2, Europe, 4000,  400, 1200, 3000, 4500, 6000, 5000, 5000, 1700);   //Germany
    *ptr_cards++ = new IncomeTaxCard(ui->pb_left_3);        //INCOME\nTAX
    *ptr_cards++ = new CityCard(ui->pb_left_4, NorthAmerica, 1500, 100, 600, 1500, 2500, 3500, 2000, 2000, 700);     //Canada
    *ptr_cards++ = new ChanceCard(ui->pb_left_5);           //CHANCE
    *ptr_cards++ = new CityCard(ui->pb_left_6, NorthAmerica,3000, 200, 1500, 2700, 4000, 5500, 4000, 4000, 1500);	//USA
    *ptr_cards++ = new CityCard(ui->pb_left_7, Asia,2500, 200, 900, 1600, 2500, 3500, 3000, 3000, 1200);	//Japan
    *ptr_cards++ = new JailCard(ui->pb_left_up);            //JAIL
    *ptr_cards++ = new CityCard(ui->pb_up_9, Asia, 6000, 700, 3000, 4300, 5500, 7500, 5000, 5000, 3000);     //Chhina
    *ptr_cards++ = new Collect500Card(ui->pb_up_10);        //COLLECT\n500
    *ptr_cards++ = new CityCard(ui->pb_up_11, SouthAmerica, 3300, 300, 1400, 2800, 4000, 5000, 4500, 4500, 1600);	//Chile
    *ptr_cards++ = new CommunityChestCard(ui->pb_up_12);    //COMMUNITY\nCHEST
    *ptr_cards++ = new CityCard(ui->pb_up_13, NorthAmerica, 4000, 400, 1500, 3000, 4500, 5500, 4500, 4500, 2000);	//Cuba
    *ptr_cards++ = new CityCard(ui->pb_up_14, Europe, 5000, 500, 1500, 3000, 4200, 5000, 4500, 4500, 2500);	//Greece
    *ptr_cards++ = new CityCard(ui->pb_up_15, Asia, 3000, 300, 1500, 2700, 4000, 5500, 4000, 4000, 1500);	//Taiwan
    *ptr_cards++ = new ClubCard(ui->pb_right_up);           //CLUB
    *ptr_cards++ = new CityCard(ui->pb_right_17, NorthAmerica, 2500, 200, 900, 1600, 2500, 3500, 3000, 3000, 1200);	//Mexico
    *ptr_cards++ = new CityCard(ui->pb_right_18, SouthAmerica, 5000, 500, 3000, 5000, 7000, 8000, 5000, 5000, 2500);	//Brazil
    *ptr_cards++ = new CityCard(ui->pb_right_19, Asia, 2500, 200, 1000, 2500, 3500, 4500, 3000, 3000, 1200);	//India
    *ptr_cards++ = new ChanceCard(ui->pb_right_20);         //CHANCE
    *ptr_cards++ = new CityCard(ui->pb_right_21, SouthAmerica, 2000, 200, 1000, 2700, 4500, 6000, 3500, 3500, 1100);	//Argentina
    *ptr_cards++ = new CityCard(ui->pb_right_22, Europe, 6500, 800, 3200, 4500, 6500, 8000, 6000, 6000, 3200);	//Switzerland
    *ptr_cards++ = new CityCard(ui->pb_right_23, Asia, 4000, 400, 2200, 3500, 5000, 6500, 4500, 4500, 2000);	//Turkey
    *ptr_cards++ = new RestRoomCard(ui->pb_right_down);     //RESTROOM
    *ptr_cards++ = new CityCard(ui->pb_down_25, SouthAmerica, 7000, 900, 3500, 5000, 7000, 8500, 6500, 6500, 3500);	//Peru
    *ptr_cards++ = new WealthTaxCard(ui->pb_down_26);       //WEALTH TAX
    *ptr_cards++ = new CityCard(ui->pb_down_27, SouthAmerica, 4500, 500, 1500, 3000, 4500, 5500, 4500, 4500, 2000);	//Venezuela
    *ptr_cards++ = new CityCard(ui->pb_down_28, Europe, 2500, 200, 1200, 2600, 3500, 5000, 3000, 3000, 1200);	//UK
    *ptr_cards++ = new CommunityChestCard(ui->pb_down_29);	//COMMUNITY CHEST
    *ptr_cards++ = new CityCard(ui->pb_down_30, NorthAmerica, 2000, 200, 800, 2000, 3000, 4500, 2500, 2500, 1000);	//Nicaragua
    *ptr_cards++ = new GoToClubCard(ui->pb_down_31);        //GO TO CLUB
}

void MainWindow::show_whos_turn()
{
    QWidget *player_info[] =
    {
        ui->player1_info, ui->player2_info,
        ui->player3_info, ui->player4_info
    };
    QString highlight_style(PLAYER_INFO_HIGHLIGHT_STYLE);
    QString normal_style(PLAYER_INFO_ENABLE_STYLE);
    for(int i=0; i<number_of_players; i++)
    {
        if(i==current_player_index)
            player_info[i]->setStyleSheet(highlight_style);
        else
            player_info[i]->setStyleSheet(normal_style);
    }
#ifdef PLATFORM_PXA270
    led_timer.led_n = current_player_index+1;
    led_timer.show_led();
#endif
}

void MainWindow::show_current_player_info(Player *current_player)
{
    ui->lb_rounds->setText(QString::number(current_player->get_round()));
    ui->lb_owned_cards->setText(QString::number(current_player->get_owned_amount()));
}

void MainWindow::pb_player_move_delay()
{
    QTime diceTime = QTime::currentTime().addMSecs(PLAYER_MOVE_DELAY);
    while (QTime::currentTime() < diceTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

void MainWindow::update_pb_player(int total_dice, int from, int *to)
{
    int index = from;
    for(int cnt=1; cnt<=total_dice; cnt++)
    {
        index = (index + 1) % 32;
        show_pb_player(index, current_player_index);
        pb_player_move_delay();
    }
    *to = index;
}

void MainWindow::show_current_card_info(int card_index)
{
    Card *current_card = cards[card_index];
    ui->lb_card_owner->setText(current_card->get_owner_name());
    ui->lb_card_name->setText(current_card->get_pb()->text());
    ui->lb_card_price->setText(current_card->get_card_price());
    ui->lb_card_type->setText(current_card->get_card_type());

    QString card_style = current_card->get_pb()->styleSheet();
    ui->group_card_info->setStyleSheet(card_style);
}

bool MainWindow::decide_who_is_next(int total_dice)
{
    if(total_dice == 12)
        return true; //Keep on this player
    current_player_index = (current_player_index+1) % number_of_players;
    return false;
}

void MainWindow::show_players_balance()
{
    QLCDNumber *balance_lcds[] =
    {
        ui->player1_balance, ui->player2_balance,
        ui->player3_balance, ui->player4_balance
    };
    for(int i=0; i<number_of_players; i++)
    {
        balance_lcds[i]->display(players[i].get_balance());
    }
}

void MainWindow::disable_all_buttons()
{
    ui->pb_buy->setEnabled(false);
    ui->pb_sell->setEnabled(false);
    ui->pb_build->setEnabled(false);
    ui->pb_roll_dice->setEnabled(false);
}

void MainWindow::on_pb_roll_dice_clicked()
{
    disable_all_buttons();
//#ifdef PLATFORM_PXA270
//    roll_dice_sound.start();
//#endif

    srand(time(NULL));
    int dice1 = (rand() % 6 + 1);
    int dice2 = (rand() % 6 + 1);
    ui->dice_display_1->setText(QString::number(dice1));
    ui->dice_display_2->setText(QString::number(dice2));
    int total_dice = dice1 + dice2;
#ifdef PLATFORM_PXA270
    led_timer.show_seg(dice1,dice2,total_dice);
#endif

    Player *current_player = &players[current_player_index];
    transaction_player_index = current_player_index;
    int dst, from = current_player->get_at_card_index();

    show_whos_turn();//TODO:PXA270 show LED
    show_current_player_info(current_player);
    update_pb_player(total_dice, from, &dst);
    show_current_card_info(dst);
    current_player->set_new_index(dst);  //TODO: GUI tell the user new round or not
    decide_who_is_next(total_dice);      //TODO: GUI tell keep on this player or not

    bool can_buy,can_sell,can_renovate;
    Card *dst_card = cards[dst];
    bool need_post = dst_card->apply(current_player, total_dice,
                                     &bank, players, number_of_players,
                                     &can_buy,&can_sell,&can_renovate);
    if(need_post)
    {
        int post_position = dst_card->get_post_position();
        show_pb_player(post_position, transaction_player_index);
        Card *post_postion_card = cards[post_position];
        post_postion_card->apply(current_player, total_dice,
                                 &bank, players, number_of_players,
                                 &can_buy,&can_sell,&can_renovate);
    }
    show_players_balance();
    show_current_player_info(current_player);

    ui->pb_buy->setEnabled(can_buy);
    ui->pb_sell->setEnabled(can_sell);
    ui->pb_build->setEnabled(can_renovate);
    ui->pb_roll_dice->setEnabled(true);
}

void MainWindow::on_pb_buy_clicked()
{
    ui->pb_buy->setEnabled(false);
    Player *transaction_player = &players[transaction_player_index];
    int transaction_card_index = transaction_player->get_at_card_index();
    Card *transaction_card = cards[transaction_card_index];
    CityCard *city_card = dynamic_cast<CityCard *>(transaction_card);
    if(!city_card)
    {
        qDebug("Imposible transaction happen!!!!! transaction_player_index=%d, card_id=%d",
               transaction_player_index, transaction_player->get_at_card_index());
        ui->pb_sell->setEnabled(true);
    }

    int price = city_card->get_price_of_city();
    if (transaction_player->get_balance() >= price) //TODO: neg after trans.
    {
        transaction_player->expenses(price);
        bank.deposit(price);
        city_card->set_owner(transaction_player);
        transaction_player->buy_card(city_card, price);
    }
    else    ui->pb_sell->setEnabled(true);
    show_current_player_info(transaction_player);
    show_current_card_info(transaction_card_index);
    show_players_balance();
}

void MainWindow::on_pb_sell_clicked()
{
    ui->pb_sell->setEnabled(false);
    Player *transaction_player = &players[transaction_player_index];
    int transaction_card_index = transaction_player->get_at_card_index();
    Card *transaction_card = cards[transaction_card_index];
    CityCard *city_card = dynamic_cast<CityCard *>(transaction_card);
    if(!city_card)
    {
        qDebug("Imposible transaction happen!!!!! transaction_player_index=%d, card_id=%d",
               transaction_player_index, transaction_player->get_at_card_index());
        ui->pb_sell->setEnabled(true);
    }

    int mortgage_price = city_card->get_mortgage_val();
    transaction_player->income(mortgage_price);
    bank.withdrawal(mortgage_price);
    transaction_player->sell_card(city_card, mortgage_price);
    city_card->set_owner(NULL);

    show_current_player_info(transaction_player);
    show_current_card_info(transaction_card_index);
    show_players_balance();
}

void MainWindow::on_pb_build_clicked()
{
    int count_of_cards_of_particular_color = 0;
    Player *transaction_player = &players[transaction_player_index];
    int transaction_card_index = transaction_player->get_at_card_index();
    Card *transaction_card = cards[transaction_card_index];
    CityCard *city_card = dynamic_cast<CityCard *>(transaction_card);
    if(!city_card)
    {
        qDebug("Imposible transaction happen!!!!! transaction_player_index=%d, card_id=%d",
               transaction_player_index, transaction_player->get_at_card_index());
    }

    int owned_amount;
    CityCard *owned_card = transaction_player->get_owned_cards(&owned_amount);
    for (int p = 0; p < owned_amount; p++, owned_card++)
    {
        //TODO: VERY BAD AND UGLY WAY
        if (owned_card->get_belong_continent() == city_card->get_belong_continent())
        {
            count_of_cards_of_particular_color++;
        }
    }

    //TODO: MONEY NOT ENOUGH case
    if (count_of_cards_of_particular_color >= 3 &&
        city_card->get_type_of_rent() <= 4)
    {
        int cost;
        if (city_card->get_type_of_rent() == 4)
        {
            cost = city_card->get_cost_of_hotel();
        }
        else
        {
            cost = city_card->get_cost_of_house();
        }
        transaction_player->expenses(cost);
        bank.deposit(cost);
        city_card->increase_type_of_rent();
    }

    show_current_player_info(transaction_player);
    show_current_card_info(transaction_card_index);
    show_players_balance();
}

#ifdef PLATFORM_PXA270
void MainWindow::set_board_square()
{
    QDesktopWidget *mydesk = new QDesktopWidget();
    QRect rect = mydesk->screenGeometry();
    int x_max = rect.width();
    int y_max = rect.height();

    int board_width, board_height;
    if (x_max / (x_max%y_max) < 3)
        board_width = board_height = y_max;
    else
        board_width = board_height = (int)(y_max*(0.9));

    qDebug("board_width=%d,board_height==%d", board_width, board_height);
    ui->board->setFixedSize(board_width, board_height);
}
#endif
