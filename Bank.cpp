#include "Bank.h"

Bank::Bank()
{
    total_funds = 1000000;
}

int Bank::get_total_funds() const
{
    return total_funds;
}

int Bank::withdrawal(int amount)
{
    total_funds -= amount;
    return total_funds;
}

int Bank::deposit(int amount)
{
    total_funds += amount;
    return total_funds;
}
