#ifndef PLAYER_H
#define PLAYER_H

#include <vector>

class CityCard;

class Player
{
private:
    char *name;
    int at_card_index;
    int balance;
    int rounds;
    std::vector<CityCard *> cards_owned;

public:
    Player();
    void add_round();
    void set_player_name(char *name);
    char *get_name() const;
    int get_round() const;
    int get_balance() const;
    int get_owned_amount() const;
    int get_at_card_index() const;
    bool set_new_index(int new_index);
    int income(int amount);
    int expenses(int amount);
    void buy_card(CityCard *card, int price_of_card);
    void sell_card(CityCard *card, int price_of_card);
    CityCard *get_owned_cards(int *amount) const;
};

#endif // PLAYER_H
