#include "Card.h"
#include "Player.h"
#include "Bank.h"

Card::Card(QAbstractButton *pb) : pb_card(pb)
{
}

QAbstractButton *Card::get_pb() const
{
    return pb_card;
}

QString Card::get_owner_name() const
{
    return "-";
}

QString Card::get_card_price() const
{
    return "-";
}

QString Card::get_card_type() const
{
    return "No Building";
}

void Card::apply_action(Player *, int , Bank *, Player *, int)
{
}

bool Card::apply(Player *current_player, int total_dice,
                 Bank *bank, Player *players, int number_of_players,
                 bool *buy, bool *sell, bool *renovate)
{
    this->apply_action(current_player, total_dice, bank, players, number_of_players);
    *buy = false;       //special card can not buy
    *sell = false;      //special card can not sell
    *renovate = false;  //special card can not renovate
    return (this->get_post_position() != -1);
}

int Card::get_post_position() const
{
    return -1;
}

CityCard::CityCard(QAbstractButton *pb, Continent continent,
                   int pricecity, int rentwithouthouse,
                   int rentwith1house, int rentwith2house, int rentwith3house,
                   int rentwithhotel,  int costofhouse, int costhotel, int mortgageval)
    : Card(pb)
{
    this->owner = NULL;
    this->belong_continent = continent;
    this->price_of_city = pricecity;
    this->rent_without_house = rentwithouthouse;
    this->rent_with_1_house = rentwith1house;
    this->rent_with_2_house = rentwith2house;
    this->rent_with_3_house = rentwith3house;
    this->rent_with_hotel = rentwithhotel;
    this->cost_of_house = costofhouse;
    this->cost_of_hotel = costhotel;
    this->mortgage_val = mortgageval;
    this->type_of_rent = 0;
}

QString CityCard::get_owner_name() const
{
    if(owner==NULL)
        return "Bank";
    return owner->get_name();
}

QString CityCard::get_card_price() const
{
    return QString::number(price_of_city);
}

QString CityCard::get_card_type() const
{
    if (type_of_rent <= 3)
        return QString::number(type_of_rent) + " House";

    return "Hotel";
}

int CityCard::get_price_of_city() const
{
    return price_of_city;
}

int CityCard::get_type_of_rent() const
{
    return type_of_rent;
}

int CityCard::get_mortgage_val() const
{
    return mortgage_val;
}

int CityCard::get_cost_of_house() const
{
    return cost_of_house;
}

int CityCard::get_cost_of_hotel() const
{
    return cost_of_hotel;
}

Continent CityCard::get_belong_continent() const
{
    return belong_continent;
}

void CityCard::increase_type_of_rent()
{
    type_of_rent++;
}

bool CityCard::apply(Player *current_player, int total_dice,
                     Bank *bank, Player *players, int number_of_players,
                     bool *buy, bool *sell, bool *renovate)
{
    if (this->owner == NULL)    //Own by bank
    {
        *buy = true;
        *sell = false;
        *renovate = false;      //TODO: apply to GUI pb
    }
    else
    {
        if (this->owner == current_player)
        {
            *buy = false;
            *sell = true;
            *renovate = true;
        }
        else
        {
            *buy = false;
            *sell = false;
            *renovate = false;
            pay_rent(current_player);
        }
    }
    return false;
}

void CityCard::set_owner(Player *new_owner)
{
    this->owner = new_owner;
}

void CityCard::pay_rent(Player *current_player) const
{
    switch(type_of_rent)
    {
        case 0:
            current_player->expenses(rent_without_house);
            owner->income(rent_without_house);
            break;
        case 1:
            current_player->expenses(rent_with_1_house);
            owner->income(rent_with_1_house);
            break;
        case 2:
            current_player->expenses(rent_with_2_house);
            owner->income(rent_with_2_house);
            break;
        case 3:
            current_player->expenses(rent_with_3_house);
            owner->income(rent_with_3_house);
            break;
        default:
            current_player->expenses(rent_with_hotel);
            owner->income(rent_with_hotel);
            break;
    }
}

int PostPositionCard::get_post_position() const
{
    return post_position;
}

void StartCard::apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players)
{
    //TODO: Check why multiply one
//    if (current_player->get_round() == 0)
//    {
//        int balance = current_player->get_balance();
//        current_player->set_new_balance(balance*1);
//    }
}

void IncomeTaxCard::apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players)
{
    int owned_amount = current_player->get_owned_amount();
    current_player->expenses(50*owned_amount);
}

void ChanceCard::apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players)
{
    post_position = -1;
    switch (total_dice)
    {
        case 2:
            current_player->expenses(2000);
            break;
        case 3:
            current_player->income(2500);
            break;
        case 4:
            current_player->expenses(1000);
            break;
        case 5:
            current_player->income(1000);
            break;
        case 6:
            current_player->expenses(1500);
            break;
        case 7:
            current_player->income(2000);
            break;
        case 8:
            current_player->expenses(3000);
            break;
        case 9:
            current_player->income(1500);
            break;
        case 10:
            post_position = JAIL_CARD_INDEX;        //TODO:post apply card needed
            break;
        case 11:
            current_player->income(3000);
            break;
        case 12:
            post_position = REST_ROOM_CARD_INDEX;   //TODO:post apply card needed
            break;
    }
}

void JailCard::apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players)
{
    current_player->expenses(500);
    bank->deposit(500);
}

void Collect500Card::apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players)
{
    bank->withdrawal(500);
    current_player->income(500);
}

void CommunityChestCard::apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players)
{
    post_position = -1;
    switch (total_dice)
    {
        case 2:
            current_player->income((number_of_players - 1) * 500);
            break;
        case 3:
            post_position = JAIL_CARD_INDEX;        //TODO:post apply card needed
            break;
        case 4:
            current_player->income(2500);
            break;
        case 5:
            current_player->expenses(1000);
            break;
        case 6:
            current_player->income(2000);
            break;
        case 7:
            current_player->expenses(2000);
            break;
        case 8:
            current_player->income(1000);
            break;
        case 9:
            current_player->expenses(500);
            break;
        case 10:
            current_player->income(1500);
            break;
        case 11:
            current_player->expenses(1500);
            break;
        case 12:
            current_player->income(3000);
            break;
    }
}

void ClubCard::apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players)
{
    current_player->expenses(100);
    for (int p = 0; p < number_of_players; p++, players++)
    {
        if (players != current_player)
        {
            players->income(100);
        }
    }
}

void RestRoomCard::apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players)
{
    current_player->income(100);
    for (int p = 0; p < number_of_players; p++, players++)
    {
        if (players != current_player)
        {
            players->expenses(100);
        }
    }
}

void WealthTaxCard::apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players)
{
    int own_n_card, a = 0, b = 0;
    CityCard *owned_cards = current_player->get_owned_cards(&own_n_card);
    for (int i = 0; i < own_n_card; i++, owned_cards++)
    {
        a += owned_cards->get_type_of_rent() % 4;
        b += owned_cards->get_type_of_rent() % 5 - a;
    }
    current_player->expenses((a * 50) + (b * 100));
}

void GoToClubCard::apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players)
{
    post_position = CLUB_CARD_INDEX;    //TODO:post apply card needed
}
