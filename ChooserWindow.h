#ifndef CHOOSERWINDOW_H
#define CHOOSERWINDOW_H
#define PLAYER1_NAME "MRS. PEACOCK"
#define PLAYER1_CHESS ":/images/images/blue_chess.png"
#define PLAYER2_NAME "MR. GREEN"
#define PLAYER2_CHESS ":/images/images/green_chess.png"
#define PLAYER3_NAME "MISS. SCARLETT"
#define PLAYER3_CHESS ":/images/images/red_chess.png"
#define PLAYER4_NAME "MRS. WHITE"
#define PLAYER4_CHESS ":/images/images/white_chess.png"
#define PLAYER5_NAME "COLONEL MUSTARD"
#define PLAYER5_CHESS ":/images/images/yellow_chess.png"
#define BG_MUSIC_NAME "sounds/background.mp3"

#include <QMainWindow>
#include "MusicThread.h"

namespace Ui {
class ChooserWindow;
}

class ChooserWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ChooserWindow(QWidget *parent = 0);
    ~ChooserWindow();

protected:
    void closeEvent(QCloseEvent *event);

public slots:
    void on_pb_go_clicked();
    void on_tb_player1_toggled(bool checked);
    void on_tb_player2_toggled(bool checked);
    void on_tb_player3_toggled(bool checked);
    void on_tb_player4_toggled(bool checked);
    void on_tb_player5_toggled(bool checked);

private:
    int checkd_count;
    char *player_names[5];
    char *player_chess[5];
    bool start;
    Ui::ChooserWindow *ui;
    MusicThread *bg_music_thread;
    void player_toggle(bool checked);
    void open_main_window(int n_player, char *player_names[4], char *player_chess[4]);
};

#endif // CHOOSERWINDOW_H
