#include "ChooserWindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ChooserWindow w;
    w.show();

    return a.exec();
}
