#ifndef BANK_H
#define BANK_H

class Bank
{
private:
    int total_funds;
public:
    Bank();
    int get_total_funds() const;
    int withdrawal(int amount);
    int deposit(int amount);
};

#endif // BANK_H
