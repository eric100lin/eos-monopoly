#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#define N_CARDS 32
#define START_CARD_INDEX 0
#define MAX_PLAYERS 4
#define PLAYER_OFFSET 5
#define PLAYER_WIDTH  20
#define PLAYER_HEIGHT 20
#define PLAYER_MOVE_DELAY 500
#define PLAYER_INFO_ENABLE_STYLE    "background-color:rgb(255,255,255)"
#define PLAYER_INFO_HIGHLIGHT_STYLE "background-color:rgb(155,155,155)"
#define ROLL_SOUND_NAME "sounds/rollthedice.mp3"
#include <QMainWindow>
#include <QAbstractButton>
#include <QPushButton>
#include <QCloseEvent>
#include "Player.h"
#include "Card.h"
#include "Bank.h"
#include "LedTimer.h"
#include "MusicThread.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(int n_player,
        char *player_names[4], char *player_chess[4],
        MusicThread *bg_music_thread, QWidget *parent = 0);
    ~MainWindow();

public slots:
    void on_pb_roll_dice_clicked();
    void on_pb_buy_clicked();
    void on_pb_sell_clicked();
    void on_pb_build_clicked();

protected:
    void closeEvent(QCloseEvent *event);

private:
    Ui::MainWindow *ui;
    int number_of_players;
    int current_player_index;
    int transaction_player_index;
    Bank bank;
    Player players[MAX_PLAYERS];
    Card *cards[N_CARDS];
    QPushButton *pb_players[MAX_PLAYERS];
    void enable_n_player_info();
    void create_players(char *player_names[4],char *player_chess[4]);
    void show_pb_player(int card_index, int player_index);
    void allocate_and_init_cards();
    void show_whos_turn();
    void show_current_player_info(Player *current_player);
    void pb_player_move_delay();
    void update_pb_player(int total_dice, int from, int *to);
    void show_current_card_info(int card_index);
    bool decide_who_is_next(int total_dice);
    void show_players_balance();
    void disable_all_buttons();

    //PXA270 specific
    LedTimer led_timer;
    MusicThread *bg_music_thread;
//    MusicThread roll_dice_sound;
#ifdef PLATFORM_PXA270
    void set_board_square();
#endif
};

#endif // MAINWINDOW_H
