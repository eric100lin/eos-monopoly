#include "MusicThread.h"
#ifdef PLATFORM_PXA270
#include "my_madplay.h"
#endif

MusicThread::MusicThread()
{
    this->stopped = 0;
}

void MusicThread::set_music_name(char *music_name, bool once)
{
    this->once = once;
    this->stopped = 0;
    this->music_name = music_name;
}

void MusicThread::stop()
{
    stopped = 1;
}

void MusicThread::run()
{
    char *argv[2];
    argv[0] = "madplay";
    argv[1] = music_name;
    do
    {
        qDebug("MusicThread::run() start to play");
#ifdef PLATFORM_PXA270
        my_madplay_main(&stopped, 2, argv);
#endif
    } while(stopped==0 && !once);
    qDebug("MusicThread::run() out~");
}
