#ifndef CARD_H
#define CARD_H
#include <QAbstractButton>
#include <QString>
#define JAIL_CARD_INDEX 8
#define REST_ROOM_CARD_INDEX 24
#define CLUB_CARD_INDEX 16

class Player;
class Bank;

class Card
{
private:
    QAbstractButton *pb_card;
protected:
    virtual void apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players);
public:
    Card(QAbstractButton *pb);
    virtual ~Card() {}
    QAbstractButton *get_pb() const;
    virtual QString get_owner_name() const;
    virtual QString get_card_price() const;
    virtual QString get_card_type() const;
    virtual bool apply(Player *current_player, int total_dice,
                       Bank *bank, Player *players, int number_of_players,
                       bool *buy, bool *sell, bool *renovate);
    virtual int get_post_position() const;
};

enum Continent { Africa, Asia, Europe, NorthAmerica, SouthAmerica };

class CityCard : public Card
{
private:
    Player *owner;
    Continent belong_continent;
    int price_of_city, rent_without_house, rent_with_1_house, rent_with_2_house, rent_with_3_house;
    int rent_with_hotel, cost_of_house, cost_of_hotel, mortgage_val;
    int type_of_rent;
    void pay_rent(Player *current_player) const;

public:
    CityCard(QAbstractButton *pb, Continent continent,
             int pricecity, int rentwithouthouse,
             int rentwith1house, int rentwith2house, int rentwith3house,
             int rentwithhotel,  int costofhouse, int costhotel, int mortgageval);
    ~CityCard() {}
    QString get_owner_name() const;
    QString get_card_price() const;
    QString get_card_type() const;
    int get_price_of_city() const;
    int get_type_of_rent() const;
    int get_mortgage_val() const;
    int get_cost_of_house() const;
    int get_cost_of_hotel() const;
    Continent get_belong_continent() const;
    void increase_type_of_rent();
    bool apply(Player *current_player, int total_dice,
               Bank *bank, Player *players, int number_of_players,
               bool *buy, bool *sell, bool *renovate);
    void set_owner(Player *new_owner);
};

class PostPositionCard : public Card
{
protected:
    int post_position;
public:
    PostPositionCard(QAbstractButton *pb) : Card(pb), post_position(-1) {}
    virtual ~PostPositionCard() {}
    virtual int get_post_position() const;
};

class StartCard : public Card
{
protected:
    virtual void apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players);
public:
    StartCard(QAbstractButton *pb) : Card(pb) {}
};

class IncomeTaxCard : public Card
{
protected:
    virtual void apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players);
public:
    IncomeTaxCard(QAbstractButton *pb) : Card(pb) {}
};

class ChanceCard : public PostPositionCard
{
protected:
    virtual void apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players);
public:
    ChanceCard(QAbstractButton *pb) : PostPositionCard(pb) {}
};

class JailCard : public Card
{
protected:
    virtual void apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players);
public:
    JailCard(QAbstractButton *pb) : Card(pb) {}
};

class Collect500Card : public Card
{
protected:
    virtual void apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players);
public:
    Collect500Card(QAbstractButton *pb) : Card(pb) {}
};

class CommunityChestCard : public PostPositionCard
{
protected:
    virtual void apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players);
public:
    CommunityChestCard(QAbstractButton *pb) : PostPositionCard(pb) {}
};

class ClubCard : public Card
{
protected:
    virtual void apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players);
public:
    ClubCard(QAbstractButton *pb) : Card(pb) {}
};

class RestRoomCard : public Card
{
protected:
    virtual void apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players);
public:
    RestRoomCard(QAbstractButton *pb) : Card(pb) {}
};

class WealthTaxCard : public Card
{
protected:
    virtual void apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players);
public:
    WealthTaxCard(QAbstractButton *pb) : Card(pb) {}
};

class GoToClubCard : public PostPositionCard
{
protected:
    virtual void apply_action(Player *current_player, int total_dice, Bank *bank, Player *players, int number_of_players);
public:
    GoToClubCard(QAbstractButton *pb) : PostPositionCard(pb) {}
};

#endif // CARD_H
