#ifndef MUSICTHREAD_H
#define MUSICTHREAD_H
#include <QThread>

class MusicThread : public QThread
{
    Q_OBJECT

public:
    MusicThread();
    void set_music_name(char *music_name, bool once=false);
    void stop();

protected:
    void run();

private:
    char *music_name;
    bool once;
    volatile int stopped;
};

#endif // MUSICTHREAD_H
