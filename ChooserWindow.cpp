#include "ChooserWindow.h"
#include "ui_ChooserWindow.h"
#include "MainWindow.h"

ChooserWindow::ChooserWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ChooserWindow)
{
    start = false;
    ui->setupUi(this);
    checkd_count = 0;
#ifdef PLATFORM_PXA270
    showMaximized();
    bg_music_thread = new MusicThread();
    bg_music_thread->set_music_name((char *)BG_MUSIC_NAME);
    bg_music_thread->start();
#endif
}

ChooserWindow::~ChooserWindow()
{
    delete ui;
}

void ChooserWindow::closeEvent(QCloseEvent *event)
{
#ifdef PLATFORM_PXA270
    if(!start)
    {
        bg_music_thread->stop();
        bg_music_thread->wait();
//        delete bg_music_thread;
    }
#endif
    event->accept();
}

void ChooserWindow::player_toggle(bool checked)
{
    if(checked)
        checkd_count++;
    else
        checkd_count--;

    bool enable = checkd_count>=2 && checkd_count<5;
    ui->pb_go->setEnabled(enable);
}

void ChooserWindow::on_tb_player1_toggled(bool checked)
{
    player_names[checkd_count] = PLAYER1_NAME;
    player_chess[checkd_count] = PLAYER1_CHESS;
    player_toggle(checked);
}

void ChooserWindow::on_tb_player2_toggled(bool checked)
{
    player_names[checkd_count] = PLAYER2_NAME;
    player_chess[checkd_count] = PLAYER2_CHESS;
    player_toggle(checked);
}

void ChooserWindow::on_tb_player3_toggled(bool checked)
{
    player_names[checkd_count] = PLAYER3_NAME;
    player_chess[checkd_count] = PLAYER3_CHESS;
    player_toggle(checked);
}

void ChooserWindow::on_tb_player4_toggled(bool checked)
{
    player_names[checkd_count] = PLAYER4_NAME;
    player_chess[checkd_count] = PLAYER4_CHESS;
    player_toggle(checked);
}

void ChooserWindow::on_tb_player5_toggled(bool checked)
{
    player_names[checkd_count] = PLAYER5_NAME;
    player_chess[checkd_count] = PLAYER5_CHESS;
    player_toggle(checked);
}

void ChooserWindow::on_pb_go_clicked()
{
    open_main_window(checkd_count, player_names, player_chess);
}

void ChooserWindow::open_main_window(int n_player, char *player_names[4], char *player_chess[4])
{
    MainWindow *w = new MainWindow(n_player,player_names, player_chess, bg_music_thread);
    w->show();
    start = true;
    this->close();
}
