#-------------------------------------------------
#
# Project created by QtCreator 2015-12-22T22:47:07
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = EOS-Monopoly
TEMPLATE = app
QMAKE_CXXFLAGS += -Wunused-parameter

SOURCES += main.cpp \
    ChooserWindow.cpp \
    MainWindow.cpp \
    Player.cpp \
    Card.cpp \
    Bank.cpp \
    LedTimer.cpp \
    MusicThread.cpp

HEADERS  += \
    ChooserWindow.h \
    MainWindow.h \
    Player.h \
    Card.h \
    Bank.h \
    LedTimer.h \
    MusicThread.h

FORMS    += \
    ChooserWindow.ui \
    MainWindow.ui

RESOURCES += \
    map.qrc

#Define platform-specific preprocessor marco
CONFIG(PXA270) {
    DEFINES += PLATFORM_PXA270 HAVE_CONFIG_H
    INCLUDEPATH += \
        /home/thlin/microtime/linux/include \
        mp3-libs/build-pxa270/include \
        mp3-libs/madplay-0.15.2b \
        mp3-libs/intl
    SOURCES += \
        mp3-libs/madplay-0.15.2b/audio_aiff.c \
        mp3-libs/madplay-0.15.2b/audio_hex.c \
        mp3-libs/madplay-0.15.2b/audio.c \
        mp3-libs/madplay-0.15.2b/audio_raw.c \
        mp3-libs/madplay-0.15.2b/audio_wave.c \
        mp3-libs/madplay-0.15.2b/filter.c \
        mp3-libs/madplay-0.15.2b/getopt.c \
        mp3-libs/madplay-0.15.2b/player.c \
        mp3-libs/madplay-0.15.2b/rgain.c \
        mp3-libs/madplay-0.15.2b/version.c \
        mp3-libs/madplay-0.15.2b/audio_cdda.c \
        mp3-libs/madplay-0.15.2b/audio_null.c \
        mp3-libs/madplay-0.15.2b/audio_oss.c \
        mp3-libs/madplay-0.15.2b/audio_snd.c \
        mp3-libs/madplay-0.15.2b/crc.c \
        mp3-libs/madplay-0.15.2b/getopt1.c \
        mp3-libs/madplay-0.15.2b/resample.c \
        mp3-libs/madplay-0.15.2b/tag.c \
        mp3-libs/madplay-0.15.2b/my_madplay.c
    LIBS += \
        -L"$$_PRO_FILE_PWD_/mp3-libs/build-pxa270/lib" -lz -lmad -lid3tag
}
