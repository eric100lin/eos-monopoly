#include "LedTimer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/fcntl.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <time.h>
#define LED_INTERVAL 500
#ifdef PLATFORM_PXA270
#include "asm-arm/arch-pxa/lib/creator_pxa270_lcd.h"
#define LCD_DEVICE_NAME "/dev/lcd"
#endif

LedTimer::LedTimer(QObject *parent) : QObject(parent)
{
    light_dark = true;
    led_n = 1;
    m_nTimerId = startTimer(LED_INTERVAL);
#ifdef PLATFORM_PXA270
    if ((fd_seg=open(LCD_DEVICE_NAME, O_RDWR))<0)
    {
        qFatal("Open LCD device[%s] fail\n", LCD_DEVICE_NAME);
        exit(-1);
    }
    qDebug("LedTimer::LedTimer(QObject *parent)");

    off_led();
#endif
}

LedTimer::~LedTimer()
{
    if(m_nTimerId != 0)
        killTimer(m_nTimerId);
#ifdef PLATFORM_PXA270
    close(fd_seg);
#endif
}

void LedTimer::timerEvent(QTimerEvent *event)
{
    light_dark = !light_dark;
    //qDebug("timer event, id %d, light_dark %d",event->timerId(),light_dark);
    if(light_dark)  show_led();
    else            off_led();
}

void LedTimer::show_led()
{
#ifdef PLATFORM_PXA270
    off_led();
    unsigned short ldata;
    switch(led_n)
    {
        case 1:
            ldata = LED_D9_INDEX;
            break;
        case 2:
            ldata = LED_D10_INDEX;
            break;
        case 3:
            ldata = LED_D11_INDEX;
            break;
        case 4:
            ldata = LED_D12_INDEX;
            break;
    }
    ioctl(fd_seg, LED_IOCTL_BIT_SET, &ldata);
#endif
}

void LedTimer::off_led()
{
#ifdef PLATFORM_PXA270
    unsigned short ldata;
    ldata = LED_ALL_OFF;
    ioctl(fd_seg, LED_IOCTL_SET, &ldata);
#endif
}

void LedTimer::show_seg(int dice1, int dice2, int total_dice)
{
#ifdef PLATFORM_PXA270
    ioctl(fd_seg, _7SEG_IOCTL_ON, NULL);
    _7seg_info_t data;
    data.Mode = _7SEG_MODE_HEX_VALUE;
    data.Which = _7SEG_ALL;
    unsigned long h1 = dice1;
    h1 <<= 12;
    unsigned long h2 = dice2;
    h2 <<= 8;
    unsigned long h3 = (((int)total_dice)/10);
    h3 <<= 4;
    unsigned long h4 = ((int)total_dice%10);
    data.Value = h1 | h2 | h3 | h4;
    ioctl(fd_seg, _7SEG_IOCTL_SET, &data);
#endif
}
